package endpoint

import (
	"net/http"

	"bitbucket.org/idomdavis/nieve/asteroid"
	"bitbucket.org/idomdavis/nieve/item"
	"bitbucket.org/idomdavis/nieve/market"
)

type oreData struct {
	item.TypeID
	Unprocessed market.Price
	Reprocessed market.Price
}

// Ore returns a page with the relative ore prices.
func Ore(w http.ResponseWriter, _ *http.Request) {
	const amount = 1000

	data := make([][]oreData, len(asteroid.Groups))

	for i, group := range asteroid.Groups {
		asteroids := make([]oreData, len(group.Variants))

		for j, variant := range group.Variants {
			asteroids[j] = oreData{
				TypeID:      variant,
				Unprocessed: market.Ask(market.Sell(variant, amount)),
				Reprocessed: market.Ask(asteroid.Value(variant, 0.683, amount)),
			}
		}

		data[i] = asteroids
	}

	Template(ore).Render(w, data)
}
