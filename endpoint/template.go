package endpoint

import (
	"bitbucket.org/idomdavis/gohttp/conversation"
	"bitbucket.org/idomdavis/gohttp/renderer"
	"github.com/sirupsen/logrus"
)

const reload = true

const (
	root      = "./dist/templates"
	layouts   = "/layouts"
	pages     = "/pages"
	extension = ".gohtml"

	base = "/base"
	ore  = "/ore"
)

var cache = map[string]*renderer.Template{}

// Template for a given page.
func Template(page string) *renderer.Template {
	if t, ok := cache[page]; ok {
		return t
	}

	t := &renderer.Template{
		Reload: reload,
		Files:  files(page),
		Error:  handler(page),
	}

	cache[page] = t

	return t
}

func files(page string) []string {
	return []string{
		root + layouts + base + extension,
		root + pages + page + extension,
	}
}

func handler(msg string) conversation.Error {
	c := make(chan error)
	h := conversation.Error{Channel: c}

	go func() {
		for err := range c {
			logrus.WithField("error", err).Errorf(msg)
		}
	}()

	return h
}
