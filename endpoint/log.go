package endpoint

import (
	"net/http"

	"github.com/sirupsen/logrus"
)

// Log a request.
func Log(r *http.Request, err ...error) {
	fields := logrus.Fields{
		"source": r.RemoteAddr,
		"Method": r.Method,
		"URL":    r.URL.Path,
	}

	switch len(err) {
	case 0:
		logrus.WithFields(fields).Info("Handled request")
	case 1:
		fields["error"] = err[0]
		logrus.WithFields(fields).Error("Error handling request")
	default:
		fields["error"] = err[0]
		fields["response error"] = err[1]
		logrus.WithFields(fields).Error("Error handling request")
	}
}
