# Ni-Eve

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/nieve/main?style=plastic)](https://bitbucket.org/idomdavis/nieve/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/nieve?style=plastic)](https://bitbucket.org/idomdavis/nieve/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/nieve?style=plastic)](https://bitbucket.org/idomdavis/nieve/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/nieve)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

Ni-Eve (pronounced naive) is a tool for providing data on industry actions in
Eve.
