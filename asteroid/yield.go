package asteroid

import "bitbucket.org/idomdavis/nieve/item"

// Yield for an asteroid.
type Yield struct {
	Tritanium int
	Pyerite   int
	Mexallon  int
	Isogen    int
	Nocxium   int
	Zydrine   int
	Megacyte  int
}

// Yields from each Ore type.
//nolint:gomnd
var Yields = map[item.TypeID]Yield{
	item.Veldspar:                {Tritanium: 400},
	item.ConcentratedVeldspar:    {Tritanium: 420},
	item.DenseVeldspar:           {Tritanium: 440},
	item.StableVeldspar:          {Tritanium: 460},
	item.Scordite:                {Tritanium: 150, Pyerite: 90},
	item.CondensedScordite:       {Tritanium: 158, Pyerite: 95},
	item.MassiveScordite:         {Tritanium: 165, Pyerite: 99},
	item.GlossyScordite:          {Tritanium: 173, Pyerite: 104},
	item.Pyroxeres:               {Pyerite: 90, Mexallon: 30},
	item.SolidPyroxeres:          {Pyerite: 95, Mexallon: 32},
	item.ViscousPyroxeres:        {Pyerite: 99, Mexallon: 33},
	item.OpulentPyroxeres:        {Pyerite: 104, Mexallon: 35},
	item.Plagioclase:             {},
	item.AzurePlagioclase:        {},
	item.RichPlagioclase:         {},
	item.SparklingPlagioclase:    {},
	item.Omber:                   {},
	item.SilveryOmber:            {},
	item.GoldenOmber:             {},
	item.PlatinoidOmber:          {},
	item.Kernite:                 {},
	item.LuminousKernite:         {},
	item.FieryKernite:            {},
	item.ResplendantKernite:      {},
	item.Jaspet:                  {},
	item.PureJaspet:              {},
	item.PristineJaspet:          {},
	item.ImmaculateJaspet:        {},
	item.Hemorphite:              {},
	item.VividHemorphite:         {},
	item.RadiantHemorphite:       {},
	item.ScintillatingHemorphite: {},
	item.Hedbergite:              {},
	item.VitricHedbergite:        {},
	item.GlazedHedbergite:        {},
	item.LustrousHedbergite:      {},
	item.Gneiss:                  {},
	item.IridescentGneiss:        {},
	item.PrismaticGneiss:         {},
	item.BrilliantGneiss:         {},
	item.DarkOchre:               {},
	item.OnyxOchre:               {},
	item.ObsidianOchre:           {},
	item.JetOchre:                {},
	item.Spodumain:               {},
	item.BrightSpodumain:         {},
	item.GleamingSpodumain:       {},
	item.DazzlingSpodumain:       {},
	item.Crokite:                 {},
	item.SharpCrokite:            {},
	item.CrystallineCrokite:      {},
	item.PellucidCrokite:         {},
	item.Bistot:                  {},
	item.TriclinicBistot:         {},
	item.MonoclinicBistot:        {},
	item.CubicBistot:             {},
	item.Arkonor:                 {},
	item.CrimsonArkonor:          {},
	item.PrimeArkonor:            {},
	item.FlawlessArkonor:         {},
	item.Mercoxit:                {},
	item.MagmaMercoxit:           {},
	item.VitreousMercoxit:        {},
}
