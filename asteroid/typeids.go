package asteroid

import "bitbucket.org/idomdavis/nieve/item"

// TypeIDs for the asteroids.
func TypeIDs() []item.TypeID {
	var ids []item.TypeID

	for _, group := range Groups {
		ids = append(ids, group.Variants...)
	}

	return ids
}
