package asteroid

import (
	"bitbucket.org/idomdavis/nieve/item"
	"bitbucket.org/idomdavis/nieve/location"
)

// Group of asteroids of a similar type.
type Group struct {
	Variants []item.TypeID
	Areas    []location.Area
}

// Groups of asteroids.
var Groups = []Group{
	{
		Variants: []item.TypeID{
			item.Veldspar,
			item.ConcentratedVeldspar,
			item.DenseVeldspar,
			item.StableVeldspar,
		},
		Areas: []location.Area{
			{Quarter: location.Amarr, Classification: location.HighSec},
			{Quarter: location.Amarr, Classification: location.NullSec},
			{Quarter: location.Caldari, Classification: location.HighSec},
			{Quarter: location.Caldari, Classification: location.NullSec},
			{Quarter: location.Gallente, Classification: location.HighSec},
			{Quarter: location.Gallente, Classification: location.NullSec},
			{Quarter: location.Minmatar, Classification: location.HighSec},
			{Quarter: location.Minmatar, Classification: location.NullSec},
		},
	},
	{
		Variants: []item.TypeID{
			item.Scordite,
			item.CondensedScordite,
			item.MassiveScordite,
			item.GlossyScordite,
		},
		Areas: []location.Area{
			{Quarter: location.Amarr, Classification: location.HighSec},
			{Quarter: location.Caldari, Classification: location.HighSec},
			{Quarter: location.Gallente, Classification: location.HighSec},
			{Quarter: location.Minmatar, Classification: location.HighSec},
		},
	},
	{
		Variants: []item.TypeID{
			item.Pyroxeres,
			item.SolidPyroxeres,
			item.ViscousPyroxeres,
			item.OpulentPyroxeres,
		},
		Areas: []location.Area{
			{Quarter: location.Amarr, Classification: location.HighSec},
			{Quarter: location.Amarr, Classification: location.LowSec},
			{Quarter: location.Amarr, Classification: location.NullSec},
			{Quarter: location.Caldari, Classification: location.HighSec},
			{Quarter: location.Caldari, Classification: location.LowSec},
			{Quarter: location.Caldari, Classification: location.NullSec},
			{Quarter: location.Wormhole, Classification: location.NullSec},
			{Quarter: location.Anomaly, Classification: location.LowSec},
			{Quarter: location.Anomaly, Classification: location.NullSec},
		},
	},
}
