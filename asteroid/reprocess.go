package asteroid

import (
	"math"

	"bitbucket.org/idomdavis/nieve/item"
	"bitbucket.org/idomdavis/nieve/market"
)

// Reprocess the asteroid at the given efficiency. Efficiency should be
// represented as a fraction, for example 0.645 for 64.5%.
func Reprocess(variant item.TypeID, efficiency float64, quantity int) Yield {
	yield := Yields[variant]

	yield.Tritanium = amount(yield.Tritanium, quantity, efficiency)
	yield.Pyerite = amount(yield.Pyerite, quantity, efficiency)
	yield.Mexallon = amount(yield.Mexallon, quantity, efficiency)
	yield.Isogen = amount(yield.Isogen, quantity, efficiency)
	yield.Nocxium = amount(yield.Nocxium, quantity, efficiency)
	yield.Zydrine = amount(yield.Zydrine, quantity, efficiency)
	yield.Megacyte = amount(yield.Megacyte, quantity, efficiency)

	return yield
}

// Value of an asteroid as measured in its component minerals. Efficiency should
// be represented as a fraction, for example 0.645 for 64.5%.
func Value(variant item.TypeID, efficiency float64, quantity int) market.Price {
	price := market.Price{}
	yield := Reprocess(variant, efficiency, quantity)

	price.Add(market.Sell(item.Tritanium, yield.Tritanium))
	price.Add(market.Sell(item.Pyerite, yield.Pyerite))
	price.Add(market.Sell(item.Mexallon, yield.Mexallon))
	price.Add(market.Sell(item.Isogen, yield.Isogen))
	price.Add(market.Sell(item.Nocxium, yield.Nocxium))
	price.Add(market.Sell(item.Zydrine, yield.Zydrine))
	price.Add(market.Sell(item.Megacyte, yield.Megacyte))

	return price
}

func amount(base, quantity int, factor float64) int {
	const batch = 100.0

	return int(math.Floor((float64(base*quantity) / batch) * factor))
}
