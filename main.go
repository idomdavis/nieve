package main

import (
	"flag"

	"bitbucket.org/idomdavis/nieve/asteroid"
	"bitbucket.org/idomdavis/nieve/item"
	"bitbucket.org/idomdavis/nieve/location"
	"bitbucket.org/idomdavis/nieve/market"
	"bitbucket.org/idomdavis/nieve/server"
	"github.com/sirupsen/logrus"
)

const itemLimit = 200

func main() {
	var port int

	flag.IntVar(&port, "port", 3333, "Server port")
	flag.Parse()

	logrus.SetFormatter(&logrus.TextFormatter{
		DisableLevelTruncation: true,
		FullTimestamp:          true,
		PadLevelText:           true,
	})

	items := append(item.Minerals, asteroid.TypeIDs()...)

	if len(items) > itemLimit {
		logrus.WithField("items", len(items)).Fatal("too many items")
	}

	if err := market.Load(location.Jita, items); err != nil {
		logrus.WithError(err).Fatal("Failed to load market data")
	}

	if err := server.Listen(port, server.Router(server.Routes)); err != nil {
		logrus.WithError(err).Error("Server exited")
	}
}
