package item

import "errors"

// TypeID is used to identify a type. These values come from Eve.
type TypeID int

// ErrInvalidTypeID indicates a type ID in the wrong format.
var ErrInvalidTypeID = errors.New("invalid type ID")

// Item type IDs.
const (
	Tritanium               = 34
	Pyerite                 = 35
	Mexallon                = 36
	Isogen                  = 37
	Nocxium                 = 38
	Zydrine                 = 39
	Megacyte                = 40
	Veldspar                = 1230
	ConcentratedVeldspar    = 17470
	DenseVeldspar           = 17471
	StableVeldspar          = 46689
	Scordite                = 1228
	CondensedScordite       = 17463
	MassiveScordite         = 17464
	GlossyScordite          = 46687
	Pyroxeres               = 1224
	SolidPyroxeres          = 17459
	ViscousPyroxeres        = 17460
	OpulentPyroxeres        = 46686
	Plagioclase             = 18
	AzurePlagioclase        = 17455
	RichPlagioclase         = 17456
	SparklingPlagioclase    = 46685
	Omber                   = 1227
	SilveryOmber            = 17867
	GoldenOmber             = 17868
	PlatinoidOmber          = 46684
	Kernite                 = 20
	LuminousKernite         = 17452
	FieryKernite            = 17453
	ResplendantKernite      = 46683
	Jaspet                  = 1226
	PureJaspet              = 17448
	PristineJaspet          = 17449
	ImmaculateJaspet        = 46682
	Hemorphite              = 1231
	VividHemorphite         = 17444
	RadiantHemorphite       = 17445
	ScintillatingHemorphite = 46681
	Hedbergite              = 21
	VitricHedbergite        = 17440
	GlazedHedbergite        = 17441
	LustrousHedbergite      = 46680
	Gneiss                  = 1229
	IridescentGneiss        = 17865
	PrismaticGneiss         = 17866
	BrilliantGneiss         = 46679
	DarkOchre               = 1232
	OnyxOchre               = 17436
	ObsidianOchre           = 17437
	JetOchre                = 46675
	Spodumain               = 19
	BrightSpodumain         = 17466
	GleamingSpodumain       = 17467
	DazzlingSpodumain       = 46688
	Crokite                 = 1225
	SharpCrokite            = 17432
	CrystallineCrokite      = 17433
	PellucidCrokite         = 46677
	Bistot                  = 1223
	TriclinicBistot         = 17428
	MonoclinicBistot        = 17429
	CubicBistot             = 46676
	Arkonor                 = 22
	CrimsonArkonor          = 17425
	PrimeArkonor            = 17426
	FlawlessArkonor         = 46678
	Mercoxit                = 11396
	MagmaMercoxit           = 17869
	VitreousMercoxit        = 17870
)

func (t TypeID) String() string {
	name, ok := Names[t]

	if !ok {
		return Unknown
	}

	return name
}
