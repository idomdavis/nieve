package item

import (
	"strconv"
	"strings"
)

// Join a set of TypeIDs into a comma separated string.
func Join(ids []TypeID) string {
	const size = 5

	if len(ids) == 0 {
		return ""
	}

	b := strings.Builder{}
	b.Grow(len(ids) * size)

	b.WriteString(strconv.Itoa(int(ids[0])))

	for _, id := range ids[1:] {
		b.WriteString(",")
		b.WriteString(strconv.Itoa(int(id)))
	}

	return b.String()
}
