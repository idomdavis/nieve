package item

// Minerals derived from Ores.
var Minerals = []TypeID{
	Tritanium,
	Pyerite,
	Mexallon,
	Isogen,
	Nocxium,
	Zydrine,
	Megacyte,
}
