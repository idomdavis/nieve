package item_test

import (
	"fmt"
	"testing"

	"bitbucket.org/idomdavis/nieve/item"
)

func ExampleJoin() {
	fmt.Println(item.Join([]item.TypeID{1, 2, 3}))

	// Output: 1,2,3
}

func TestJoin(t *testing.T) {
	t.Run("empty slice will return empty string", func(t *testing.T) {
		if s := item.Join([]item.TypeID{}); s != "" {
			t.Errorf("Expected empty string, got %q", s)
		}
	})

	t.Run("Single entry should return that entry", func(t *testing.T) {
		const expect = "1"
		if s := item.Join([]item.TypeID{1}); s != expect {
			t.Errorf("Expected %q, got %q", expect, s)
		}
	})
}
