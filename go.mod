module bitbucket.org/idomdavis/nieve

go 1.16

require (
	bitbucket.org/idomdavis/gohttp v0.4.3
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.8.1
)
