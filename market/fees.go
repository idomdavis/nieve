package market

// Fees and taxes.
const (
	BrokersFee = 0.0189
	SalesTax   = 0.025
)
