package market

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/idomdavis/nieve/item"
)

// Quotes loaded from the market.
var Quotes = map[item.TypeID]Quote{}

const (
	marketeer = "https://api.evemarketer.com/ec/marketstat/json"
)

// Load a set of market data.
func Load(market int, items []item.TypeID) error {
	var data []Data

	url := fmt.Sprintf("%s?usesystem=%d&typeid=%s",
		marketeer, market, item.Join(items))

	//nolint:gosec
	r, err := http.Get(url)

	if err != nil {
		return fmt.Errorf("failed to get market data: %w", err)
	}

	defer func() { _ = r.Body.Close() }()

	decoder := json.NewDecoder(r.Body)

	if err = decoder.Decode(&data); err != nil {
		return fmt.Errorf("failed to read market data: %w", err)
	}

	for _, e := range data {
		Quotes[e.TypeID] = e.Quote
	}

	return nil
}
