package market

// Price of an item.
type Price struct {
	WeightedAverage float64 `json:"wavg"`
	Average         float64 `json:"avg"`
	Min             float64 `json:"min"`
	Max             float64 `json:"max"`
	Variance        float64 `json:"variance"`
	StdDev          float64 `json:"stdDev"`
	Median          float64 `json:"median"`
	FivePercent     float64 `json:"fivePercent"`
}

// Add a Price to this Price.
func (p *Price) Add(price Price) {
	p.WeightedAverage += price.WeightedAverage
	p.Average += price.Average
	p.Min += price.Min
	p.Max += price.Max
	p.Variance += price.Variance
	p.StdDev += price.StdDev
	p.Median += price.Median
	p.FivePercent += price.FivePercent
}

// Multiply a Price by a given factor.
func (p *Price) Multiply(factor float64) {
	p.WeightedAverage *= factor
	p.Average *= factor
	p.Min *= factor
	p.Max *= factor
	p.Variance *= factor
	p.StdDev *= factor
	p.Median *= factor
	p.FivePercent *= factor
}
