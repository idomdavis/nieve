package market

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/idomdavis/nieve/item"
)

// Quote for an item.
type Quote struct {
	Bid Stats
	Ask Stats
}

// Data for an item returned from Marketeer. This form is used to capture the
// TypeID from Marketer.
type Data struct {
	TypeID item.TypeID
	Quote
}

// Stats for a market item (either Buy or Sell).
type Stats struct {
	Generated int `json:"generated"`
	Volume    int `json:"volume"`
	Price
}

type data struct {
	Bid stats `json:"buy"`
	Ask stats `json:"sell"`
}

type stats struct {
	Query query `json:"forQuery"`
	Stats
}

type query struct {
	Types []int `json:"types"`
}

// UnmarshalJSON takes the Eve Marketeer format and converts it into a Quote.
func (d *Data) UnmarshalJSON(b []byte) error {
	var in data

	if err := json.Unmarshal(b, &in); err != nil {
		return fmt.Errorf("invalid data format: %w", err)
	}

	if len(in.Bid.Query.Types) != 1 {
		return item.ErrInvalidTypeID
	}

	d.TypeID = item.TypeID(in.Bid.Query.Types[0])
	d.Bid = in.Bid.Stats
	d.Ask = in.Ask.Stats

	return nil
}

func (d *Data) String() string {
	b, _ := json.MarshalIndent(d, "", "  ")
	return string(b)
}
