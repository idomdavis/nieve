package market_test

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/idomdavis/nieve/market"
)

func ExampleData_UnmarshalJSON() {
	var data market.Data

	in := []byte(`
		{
			"buy": {
			  "forQuery": {
				"bid": true,
				"types": [
				  44992
				]
			  },
			  "volume": 963208,
			  "wavg": 3204519.59,
			  "avg": 3352708.11,
			  "min": 2601400.03,
			  "max": 10000000,
			  "variance": 769815422537.24,
			  "stdDev": 877391.25,
			  "median": 3094978.97,
			  "fivePercent": 2949353.31,
			  "highToLow": true,
			  "generated": 1499280224508
			},
			"sell": {
			  "forQuery": {
				"bid": true,
				"types": [
				  44992
				]
			  },
			  "volume": 963208,
			  "wavg": 3204519.59,
			  "avg": 3352708.11,
			  "min": 2601400.03,
			  "max": 10000000,
			  "variance": 769815422537.24,
			  "stdDev": 877391.25,
			  "median": 3094978.97,
			  "fivePercent": 2949353.31,
			  "highToLow": true,
			  "generated": 1499280224508
			}
		}
	`)

	if err := json.Unmarshal(in, &data); err != nil {
		fmt.Println(err)
	}

	fmt.Println(data.String())

	// Output:
	// {
	//   "TypeID": 44992,
	//   "Bid": {
	//     "generated": 1499280224508,
	//     "volume": 963208,
	//     "wavg": 3204519.59,
	//     "avg": 3352708.11,
	//     "min": 2601400.03,
	//     "max": 10000000,
	//     "variance": 769815422537.24,
	//     "stdDev": 877391.25,
	//     "median": 3094978.97,
	//     "fivePercent": 2949353.31
	//   },
	//   "Ask": {
	//     "generated": 1499280224508,
	//     "volume": 963208,
	//     "wavg": 3204519.59,
	//     "avg": 3352708.11,
	//     "min": 2601400.03,
	//     "max": 10000000,
	//     "variance": 769815422537.24,
	//     "stdDev": 877391.25,
	//     "median": 3094978.97,
	//     "fivePercent": 2949353.31
	//   }
	// }
}
