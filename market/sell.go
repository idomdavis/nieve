package market

import "bitbucket.org/idomdavis/nieve/item"

// Sell a number of items, yielding a price after fees.
func Sell(i item.TypeID, qty int) Price {
	price := Quotes[i].Bid.Price
	price.Multiply(float64(qty))

	return price
}

// Ask Price after tax.
func Ask(price Price) Price {
	price.Multiply(1 - BrokersFee)
	price.Multiply(1 - SalesTax)

	return price
}
