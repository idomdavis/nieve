package location

// System ID.
type System int

// Quarter of New Eden.
type Quarter string

// Classification of an area of space.
type Classification string

// Area in New Eden.
type Area struct {
	Quarter
	Classification
}

const (
	// Jita 4.4.
	Jita = 30000142
)

// The four New Eden "Quarters" plus the new parts of space.
const (
	Amarr    = "Amarr"
	Caldari  = "Caldari"
	Gallente = "Gallente"
	Minmatar = "Minmatar"
	Anomaly  = "Anomaly"
	Wormhole = "Wormhole"
)

// Classifications.
const (
	HighSec = "High Sec"
	LowSec  = "Low Sec"
	NullSec = "Null Sec"
)
