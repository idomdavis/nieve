package server

import "net/http"

// File defines the path of a dist file to be served by the server.
type File string

// Handler will server the defined file.
func (f File) Handler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, string(f))
}
