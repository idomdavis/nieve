package server

import (
	"fmt"
	"net/http"
	"os"
	"path"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

const static = "./dist/html"

// Router builds the http router from the given set of routes.
func Router(routes map[string]Handlers) http.Handler {
	router := mux.NewRouter()

	files, err := os.ReadDir(static)

	if err != nil {
		logrus.WithError(err).Error("Failed to read static files")
	}

	for _, file := range files {
		if file.IsDir() {
			prefix := fmt.Sprintf("/%s/", file.Name())
			dir := path.Join(static, file.Name())
			router.PathPrefix(prefix).Handler(http.StripPrefix(prefix,
				http.FileServer(http.Dir(dir)))).Methods(http.MethodGet)
		} else {
			name := "/" + file.Name()
			p := static + name
			router.HandleFunc(name, File(p).Handler).Methods(http.MethodGet)
		}
	}

	for method, handlers := range routes {
		for p, h := range handlers {
			router.HandleFunc(p, h).Methods(method)
		}
	}

	return router
}
