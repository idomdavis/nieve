package server

import (
	"net/http"

	"bitbucket.org/idomdavis/nieve/endpoint"
)

// Handlers defines a set of Gorilla URL paths and their Handler.
type Handlers map[string]http.HandlerFunc

// Routes allows the API to be defined.
var Routes = map[string]Handlers{
	http.MethodGet: map[string]http.HandlerFunc{
		"/ping":   endpoint.Ping,
		"/coffee": endpoint.Coffee,
		"/ore":    endpoint.Ore,
	},
}
